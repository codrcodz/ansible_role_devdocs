Ansible Role DevDocs
=========

This role will configure a Docker container with an Ubuntu 18.04 LTS VM containing a webserver. The webserver hosts a mirror of the contents located at [devdocs website](https://devdocs.io).

Requirements
------------

Ansible 2.8
Molecule (for testing)

Role Variables
--------------

TBD

Dependencies
------------

TBD

Example Playbook
----------------

TBD

License
-------

See "LICENSE" file.

Author Information
------------------

Dreamer Labs
